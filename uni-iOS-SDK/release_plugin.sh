#!/bin/sh

CLIENT_PLUGIN_NAME=WFClientUniPlugin
UIKIT_PLUGIN_NAME=WFUIKitUniPlugin
BUILD_DIR=build_tmp_path

#编译client plugin
cd HBuilder-uniPluginDemo/$CLIENT_PLUGIN_NAME

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

OUTPUT_FOLDER=../../../nativeplugins/wf-uni-wfc-client/ios
rm -rf $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

#分别编译模拟器和真机的Framework
xcodebuild -target ${CLIENT_PLUGIN_NAME} ONLY_ACTIVE_ARCH=YES -arch arm64 -configuration Release -sdk iphoneos BUILD_DIR="${BUILD_DIR}" clean build
xcodebuild -create-xcframework -framework "${BUILD_DIR}"/Release-iphoneos/"${CLIENT_PLUGIN_NAME}".framework -output "${OUTPUT_FOLDER}"/${CLIENT_PLUGIN_NAME}.xcframework

cp -af ../WF_SDK/Bugly.framework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/WFChatClient.xcframework "${OUTPUT_FOLDER}"/
rm -rf $BUILD_DIR

cd ../../


#编译uikit plugin
cd HBuilder-uniPluginDemo/$UIKIT_PLUGIN_NAME

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

OUTPUT_FOLDER=../../../nativeplugins/wf-uni-wfc-uikit/ios
rm -rf $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

#分别编译模拟器和真机的Framework
xcodebuild -target ${UIKIT_PLUGIN_NAME} ONLY_ACTIVE_ARCH=YES -arch arm64 -configuration Release -sdk iphoneos BUILD_DIR="${BUILD_DIR}" clean build
xcodebuild -create-xcframework -framework "${BUILD_DIR}"/Release-iphoneos/"${UIKIT_PLUGIN_NAME}".framework -output "${OUTPUT_FOLDER}"/${UIKIT_PLUGIN_NAME}.xcframework
rm -rf $BUILD_DIR

cp -af ../WF_SDK/ring.caf "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/SDWebImage.xcframework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/WebRTC.xcframework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/WFAVEngineKit.xcframework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/WFChatUIKit.xcframework "${OUTPUT_FOLDER}"/
cp -af ../WF_SDK/ZLPhotoBrowser.xcframework "${OUTPUT_FOLDER}"/


open $OUTPUT_FOLDER

cd ../../
