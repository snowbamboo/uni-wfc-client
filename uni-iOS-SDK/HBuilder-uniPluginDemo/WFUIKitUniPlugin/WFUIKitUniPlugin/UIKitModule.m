//
//  UIKitModule.m
//  WFUIKitUniPlugin
//
//  Created by Rain on 2022/7/16.
//

#import "UIKitModule.h"
#import <WFAVEngineKit/WFAVEngineKit.h>
#import <WebRTC/WebRTC.h>
#import <WFChatClient/WFCChatClient.h>
#import <WFChatUIKit/WFChatUIKit.h>

@interface WFUIKitModule () <WFAVEngineDelegate>
@property(nonatomic, strong) AVAudioPlayer *audioPlayer;
@property(nonatomic, strong) UILocalNotification *localCallNotification;
@end

@implementation WFUIKitModule
//
//@UniJSMethod(uiThread = false)
//public void enableNativeNotification(boolean enable) {
//    WfcUIKit.getWfcUIKit().setEnableNativeNotification(enable);
//}
- (instancetype)init {
    self = [super init];
    if(self) {
        [WFAVEngineKit notRegisterVoipPushService];
        [WFAVEngineKit sharedEngineKit].delegate = self;
    }
    return self;
}
UNI_EXPORT_METHOD_SYNC(@selector(enableNativeNotification:))
- (void)enableNativeNotification:(BOOL)enable {
    
}
//@UniJSMethod(uiThread = false)
//public void addICEServer(String url, String name, String password) {
//    AVEngineKit.Instance().addIceServer(url, name, password);
//}
UNI_EXPORT_METHOD_SYNC(@selector(addICEServer:name:password:))
- (void)addICEServer:(NSString *)url name:(NSString *)name password:(NSString *)password {
    [[WFAVEngineKit sharedEngineKit] addIceServer:url userName:name password:password];
}
//@UniJSMethod(uiThread = true)
//public void startConversation(String strConv, String title) {
//    Conversation conversation = parseObject(strConv, Conversation.class);
//    Context context = mUniSDKInstance.getContext();
//    Intent intent = new Intent(context, ConversationActivity.class);
//    intent.putExtra("conversation", conversation);
//    intent.putExtra("conversationTitle", title);
//    context.startActivity(intent);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startConversation:title:))
- (void)startConversation:(NSString *)strConv title:(NSString *)title {
    WFCUMessageListViewController *mvc = [[WFCUMessageListViewController alloc] init];
    mvc.conversation = [self conversationFromJsonString:strConv];
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:mvc animated:YES completion:nil];
}
//@UniJSMethod(uiThread = true)
//public void startSingleCall(String target, boolean audioOnly) {
//    WfcUIKit.singleCall(mUniSDKInstance.getContext(), target, audioOnly);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startSingleCall:audioOnly:))
- (void)startSingleCall:(NSString *)userId audioOnly:(BOOL)audioOnly {
    dispatch_async(dispatch_get_main_queue(), ^{
        WFCUVideoViewController *videoVC = [[WFCUVideoViewController alloc] initWithTargets:@[userId] conversation:[WFCCConversation conversationWithType:Single_Type target:userId line:0] audioOnly:audioOnly];
        [[WFAVEngineKit sharedEngineKit] presentViewController:videoVC];
    });
}
//@UniJSMethod(uiThread = true)
//public void startMultiCall(String groupId, List<String> participants, boolean audioOnly) {
//    WfcUIKit.multiCall(mUniSDKInstance.getContext(), groupId, participants, audioOnly);
//}
UNI_EXPORT_METHOD_SYNC(@selector(startMultiCall:participants:audioOnly:))
- (void)startMultiCall:(NSString *)groupId participants:(NSArray *)participants audioOnly:(BOOL)audioOnly {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *videoVC = [[WFCUMultiVideoViewController alloc] initWithTargets:participants conversation:[WFCCConversation conversationWithType:Group_Type target:groupId line:0] audioOnly:audioOnly];
        [[WFAVEngineKit sharedEngineKit] presentViewController:videoVC];
    });
}
//@UniJSMethod(uiThread = true)
//public void startConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean record, String callExtra) {
//    AVEngineKit.CallSession session = AVEngineKit.Instance().startConference(callId, audioOnly, pin, host, title, desc, audience, advance, record, null);
//    if (session != null) {
//        Intent intent = new Intent(mUniSDKInstance.getContext(), ConferenceActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mUniSDKInstance.getContext().startActivity(intent);
//    } else {
//        Toast.makeText(mUniSDKInstance.getContext(), "创建会议失败", Toast.LENGTH_SHORT).show();
//    }
//}
UNI_EXPORT_METHOD_SYNC(@selector(startConference:audioOnly:pin:host:title:desc:audience:advance:record:extra:))
- (void)startConference:(NSString *)callId audioOnly:(BOOL)audioOnly pin:(NSString *)pin host:(NSString *)host title:(NSString *)title desc:(NSString *)desc audience:(BOOL)audience advance:(BOOL)advance record:(BOOL)record extra:(NSString *)extra {
    WFCUConferenceViewController *vc = [[WFCUConferenceViewController alloc] initWithCallId:callId audioOnly:audioOnly pin:pin host:host.length?host:[WFCCNetworkService sharedInstance].userId title:title desc:desc audience:audience advanced:advance record:record moCall:YES extra:extra];
    [[WFAVEngineKit sharedEngineKit] presentViewController:vc];
}
//@UniJSMethod(uiThread = true)
//public void joinConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean muteAudio, boolean muteVideo, String extra) {
//    AVEngineKit.Instance().joinConference(callId, audioOnly, pin, host, title, desc, audience, advance, muteAudio, muteVideo, null);
//    Intent intent = new Intent(mUniSDKInstance.getContext(), ConferenceActivity.class);
//    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//    mUniSDKInstance.getContext().startActivity(intent);
//}
UNI_EXPORT_METHOD_SYNC(@selector(joinConference:audioOnly:pin:host:title:desc:audience:advance:muteAudio:muteVideo:extra:))
- (void)joinConference:(NSString *)callId audioOnly:(BOOL)audioOnly pin:(NSString *)pin host:(NSString *)host title:(NSString *)title desc:(NSString *)desc audience:(BOOL)audience advance:(BOOL)advance muteAudio:(BOOL)muteAudio muteVideo:(BOOL)muteVideo extra:(NSString *)extra {
    WFCUConferenceViewController *vc = [[WFCUConferenceViewController alloc] initJoinConference:callId audioOnly:audioOnly pin:pin host:host title:title desc:desc audience:audience advance:advance muteAudio:muteAudio muteVideo:muteVideo extra:extra];
    [[WFAVEngineKit sharedEngineKit] presentViewController:vc];
}
//@UniJSMethod(uiThread = false)
//public boolean isSupportMultiCall() {
//    return AVEngineKit.isSupportMultiCall();
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSupportMultiCall))
- (BOOL)isSupportMultiCall {
    return YES;
}
//@UniJSMethod(uiThread = false)
//public boolean isSupportConference() {
//    return AVEngineKit.isSupportConference();
//}
UNI_EXPORT_METHOD_SYNC(@selector(isSupportConference))
- (BOOL)isSupportConference {
    return YES;
}

- (WFCCConversation *)conversationFromJsonString:(NSString *)strConv {
    NSError *__error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[strConv dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:kNilOptions
                                                                 error:&__error];
    WFCCConversation *conversation = [[WFCCConversation alloc] init];
    if (!__error) {
        conversation.type = [dictionary[@"type"] intValue];
        conversation.target = dictionary[@"target"];
        conversation.line = [dictionary[@"line"] intValue];
    }
    return conversation;
}

#pragma mark - WFAVEngineDelegate
- (void)didReceiveCall:(WFAVCallSession *)session {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateIncomming && [WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateConnected && [WFAVEngineKit sharedEngineKit].currentSession.state != kWFAVEngineStateConnecting) {
            return;
        }
        
        UIViewController *videoVC;
        if (session.conversation.type == Group_Type && [WFAVEngineKit sharedEngineKit].supportMultiCall) {
            videoVC = [[WFCUMultiVideoViewController alloc] initWithSession:session];
        } else {
            videoVC = [[WFCUVideoViewController alloc] initWithSession:session];
        }
        
        [[WFAVEngineKit sharedEngineKit] presentViewController:videoVC];
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if([[WFCCIMService sharedWFCIMService] isVoipNotificationSilent]) {
                NSLog(@"用户设置禁止voip通知，忽略来电提醒");
                return;
            }
            if(self.localCallNotification) {
                [[UIApplication sharedApplication] scheduleLocalNotification:self.localCallNotification];
            }
            self.localCallNotification = [[UILocalNotification alloc] init];
            
            self.localCallNotification.alertBody = @"来电话了";
            
                WFCCUserInfo *sender = [[WFCCIMService sharedWFCIMService] getUserInfo:session.inviter refresh:NO];
                if (sender.displayName) {
                    if (@available(iOS 8.2, *)) {
                        self.localCallNotification.alertTitle = sender.displayName;
                    } else {
                        // Fallback on earlier versions
                        
                    }
                }
            
            self.localCallNotification.soundName = @"ring.caf";
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] scheduleLocalNotification:self.localCallNotification];
            });
        } else {
            self.localCallNotification = nil;
        }
    });
}

- (void)shouldStartRing:(BOOL)isIncoming {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateIncomming || [WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateOutgoing) {
            if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
                if([[WFCCIMService sharedWFCIMService] isVoipNotificationSilent]) {
                    NSLog(@"用户设置禁止voip通知，忽略来电震动");
                    return;
                }
                AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate, NULL, NULL, systemAudioCallback, NULL);
                AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
            } else {
                AVAudioSession *audioSession = [AVAudioSession sharedInstance];
                //默认情况按静音或者锁屏键会静音
                [audioSession setCategory:AVAudioSessionCategorySoloAmbient error:nil];
                [audioSession setActive:YES error:nil];
                
                if (self.audioPlayer) {
                    [self shouldStopRing];
                }
                
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"ring" withExtension:@"caf"];
                NSError *error = nil;
                self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                if (!error) {
                    self.audioPlayer.numberOfLoops = -1;
                    self.audioPlayer.volume = 1.0;
                    [self.audioPlayer prepareToPlay];
                    [self.audioPlayer play];
                }
            }
        }
    });
}

void systemAudioCallback (SystemSoundID soundID, void* clientData) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if ([WFAVEngineKit sharedEngineKit].currentSession.state == kWFAVEngineStateIncomming) {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
        }
    });
}

- (void)shouldStopRing {
    if (self.audioPlayer) {
        [self.audioPlayer stop];
        self.audioPlayer = nil;
        [[AVAudioSession sharedInstance] setActive:NO withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    }
}

- (void)didCallEnded:(WFAVCallEndReason) reason duration:(int)callDuration {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            if(self.localCallNotification) {
                [[UIApplication sharedApplication] cancelLocalNotification:self.localCallNotification];
                self.localCallNotification = nil;
            }
            
            if(reason == kWFAVCallEndReasonTimeout || (reason == kWFAVCallEndReasonRemoteHangup && callDuration == 0)) {
                UILocalNotification *callEndNotification = [[UILocalNotification alloc] init];
                if(reason == kWFAVCallEndReasonTimeout) {
                    callEndNotification.alertBody = @"来电未接听";
                } else {
                    callEndNotification.alertBody = @"来电已取消";
                }
                if (@available(iOS 8.2, *)) {
                    self.localCallNotification.alertTitle = @"网络通话";
                    if([WFAVEngineKit sharedEngineKit].currentSession.inviter) {
                        WFCCUserInfo *sender = [[WFCCIMService sharedWFCIMService] getUserInfo:[WFAVEngineKit sharedEngineKit].currentSession.inviter refresh:NO];
                        if (sender.displayName) {
                            self.localCallNotification.alertTitle = sender.displayName;
                        }
                    }
                }
                
                //应该播放挂断的声音
    //            self.localCallNotification.soundName = @"ring.caf";
                [[UIApplication sharedApplication] scheduleLocalNotification:callEndNotification];
            }
        }
    });
}

- (void)didReceiveIncomingPushWithPayload:(PKPushPayload * _Nonnull)payload forType:(NSString * _Nonnull)type {
    
}


@end
