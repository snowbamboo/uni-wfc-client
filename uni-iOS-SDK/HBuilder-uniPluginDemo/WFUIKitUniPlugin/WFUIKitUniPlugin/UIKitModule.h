//
//  UIKitModule.h
//  WFUIKitUniPlugin
//
//  Created by Rain on 2022/7/16.
//

#import <Foundation/Foundation.h>
#import "DCUniModule.h"

NS_ASSUME_NONNULL_BEGIN

@interface WFUIKitModule : DCUniModule

@end

NS_ASSUME_NONNULL_END
