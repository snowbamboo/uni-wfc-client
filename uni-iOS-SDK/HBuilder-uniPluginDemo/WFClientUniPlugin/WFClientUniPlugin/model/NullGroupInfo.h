//
//  NullGroupInfo.h
//  WFClientUniPlugin
//
//  Created by Rain on 2022/6/3.
//  Copyright © 2022 DCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WFChatClient/WFCChatClient.h>

NS_ASSUME_NONNULL_BEGIN

@interface NullGroupInfo : WFCCJsonSerializer
@property(nonatomic, strong)NSString *groupId;
+ (instancetype)nullGroupOfId:(NSString *)groupId;
@end

NS_ASSUME_NONNULL_END
