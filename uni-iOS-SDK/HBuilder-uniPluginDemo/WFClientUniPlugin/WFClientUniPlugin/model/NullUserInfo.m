//
//  NullUserInfo.m
//  WFClientUniPlugin
//
//  Created by Rain on 2022/6/3.
//  Copyright © 2022 DCloud. All rights reserved.
//

#import "NullUserInfo.h"

@implementation NullUserInfo
+ (instancetype)nullUserOfId:(NSString *)userId {
    NullUserInfo *userInfo = [[NullUserInfo alloc] init];
    userInfo.userId = userId;
    return userInfo;;
}
-(id)toJsonObj {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    dict[@"uid"] = self.userId;
    dict[@"name"] = @"用户";
    dict[@"displayName"] = self.userId;
    
    return dict;
}
@end
