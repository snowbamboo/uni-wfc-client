//
//  NullUserInfo.h
//  WFClientUniPlugin
//
//  Created by Rain on 2022/6/3.
//  Copyright © 2022 DCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WFChatClient/WFCChatClient.h>

NS_ASSUME_NONNULL_BEGIN

@interface NullUserInfo : WFCCJsonSerializer
@property(nonatomic, strong)NSString *userId;
+ (instancetype)nullUserOfId:(NSString *)userId;
@end

NS_ASSUME_NONNULL_END
