//
//  WindSDK.h
//  WindSDK
//
//  Created by Codi on 2021/10/19.
//

#import <Foundation/Foundation.h>

//! Project version number for WindSDK.
FOUNDATION_EXPORT double WindSDKVersionNumber;

//! Project version string for WindSDK.
FOUNDATION_EXPORT const unsigned char WindSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WindSDK/PublicHeader.h>


#import <WindSDK/WindAds.h>
#import <WindSDK/WindAdOptions.h>
#import <WindSDK/WindEnum.h>
#import <WindSDK/WindConstant.h>
#import <WindSDK/WindDislikeWords.h>
#import <WindSDK/WindAdRequest.h>
#import <WindSDK/WindRewardInfo.h>
#import <WindSDK/WindRewardVideoAd.h>
#import <WindSDK/WindIntersititialAd.h>
#import <WindSDK/WindNativeAd.h>
#import <WindSDK/WindNativeAdsManager.h>
#import <WindSDK/WindNativeAdView.h>
#import <WindSDK/WindMediaView.h>
#import <WindSDK/WindSplashAdView.h>
