package cn.wildfirechat.uni.uikit;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

import cn.wildfire.chat.kit.WfcUIKit;
import cn.wildfire.chat.kit.conversation.ConversationActivity;
import cn.wildfire.chat.kit.voip.conference.ConferenceActivity;
import cn.wildfirechat.avenginekit.AVEngineKit;
import cn.wildfirechat.model.Conversation;
import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.common.UniModule;

/**
 * 野火IM Android uikit扩展
 */
public class UIKitModule extends UniModule {

    private static final String TAG = "WFCUIKit";


//    @Override
//    public void onActivityCreate() {
//        WfcUIKit.getWfcUIKit().init();
//    }

    /**
     * sdk后处理
     */
    @Override
    public void onActivityDestroy() {
        super.onActivityDestroy();
        // 删除监听器
        Log.i(TAG, "应用销毁后处理");
    }

    @UniJSMethod(uiThread = false)
    public void enableNativeNotification(boolean enable) {
        WfcUIKit.getWfcUIKit().setEnableNativeNotification(enable);
    }

    @UniJSMethod(uiThread = false)
    public void addICEServer(String url, String name, String password) {
        AVEngineKit.Instance().addIceServer(url, name, password);
    }

    @UniJSMethod(uiThread = true)
    public void startConversation(String strConv, String title) {
        Conversation conversation = parseObject(strConv, Conversation.class);
        Context context = mUniSDKInstance.getContext();
        Intent intent = new Intent(context, ConversationActivity.class);
        intent.putExtra("conversation", conversation);
        intent.putExtra("conversationTitle", title);
        context.startActivity(intent);
    }

    @UniJSMethod(uiThread = true)
    public void startSingleCall(String target, boolean audioOnly) {
        WfcUIKit.singleCall(mUniSDKInstance.getContext(), target, audioOnly);
    }

    @UniJSMethod(uiThread = true)
    public void startMultiCall(String groupId, List<String> participants, boolean audioOnly) {
        WfcUIKit.multiCall(mUniSDKInstance.getContext(), groupId, participants, audioOnly);
    }

    @UniJSMethod(uiThread = true)
    public void startConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean record, String callExtra) {
        AVEngineKit.CallSession session = AVEngineKit.Instance().startConference(callId, audioOnly, pin, host, title, desc, audience, advance, record, null);
        if (session != null) {
            Intent intent = new Intent(mUniSDKInstance.getContext(), ConferenceActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mUniSDKInstance.getContext().startActivity(intent);
        } else {
            Toast.makeText(mUniSDKInstance.getContext(), "创建会议失败", Toast.LENGTH_SHORT).show();
        }
    }

    @UniJSMethod(uiThread = true)
    public void joinConference(String callId, boolean audioOnly, String pin, String host, String title, String desc, boolean audience, boolean advance, boolean muteAudio, boolean muteVideo, String extra) {
        AVEngineKit.Instance().joinConference(callId, audioOnly, pin, host, title, desc, audience, advance, muteAudio, muteVideo, null);
        Intent intent = new Intent(mUniSDKInstance.getContext(), ConferenceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mUniSDKInstance.getContext().startActivity(intent);
    }

    @UniJSMethod(uiThread = false)
    public boolean isSupportMultiCall() {
        return AVEngineKit.isSupportMultiCall();
    }

    @UniJSMethod(uiThread = false)
    public boolean isSupportConference() {
        return AVEngineKit.isSupportConference();
    }

    static <T> T parseObject(String text, Class<T> clazz) {
        try {
            return JSONObject.parseObject(text, clazz);
        } catch (Exception e) {
            Log.e(TAG, "parseObject exception " + clazz.getName());
        }
        return null;
    }
}
