package cn.wildfirechat.uni.uikit;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import cn.wildfire.chat.kit.Config;
import cn.wildfire.chat.kit.WfcUIKit;
import io.dcloud.feature.uniapp.UniAppHookProxy;

public class UIKitUniAppHookProxy implements UniAppHookProxy {

    private static final String TAG = "UIKitUniAppHookProxy";

    @Override
    public void onSubProcessCreate(Application application) {
        // TODO
    }

    @Override
    public void onCreate(Application application) {
        Log.e(TAG, "UIKitUniAppHook onCreate");

        WfcUIKit.getWfcUIKit().init(application);
        setupWFCDirs(application);
    }

    private void setupWFCDirs(Application application) {
        Config.VIDEO_SAVE_DIR = application.getDir("video", Context.MODE_PRIVATE).getAbsolutePath();
        Config.AUDIO_SAVE_DIR = application.getDir("audio", Context.MODE_PRIVATE).getAbsolutePath();
        Config.PHOTO_SAVE_DIR = application.getDir("photo", Context.MODE_PRIVATE).getAbsolutePath();
        Config.FILE_SAVE_DIR = application.getDir("file", Context.MODE_PRIVATE).getAbsolutePath();
    }
}

